# Actors involved:
1.	Admin
2.	Customer
3.	Chef

# Use cases:
## Admin:
1.	Login
2.	Update details
3.	Create menu list
4.	Update menu list
5.	Billing

## Customer:
1.	Login/Signup 
2.	Update details
3.	View items
4.	Select item
5.	Check item availability 
6.	Check waiting time
7.	Place order
8.	Cancel order
9.	Cancel single item
10.	Give feedback

## Chef:
1.	Login
2.	Update Details
3.	View orders
4.	Update expected time
5.	Cook food
6.	Deliver food

# Highlights and Assumptions 
-	As per your latest mail what I understood is, I have to work on the time manipulation to find the fully ordered time.
-	From the sample input I assumes that we are considering the longest waiting time as the time taken to deliver the order. I didn’t find any separate waiting time for snacks and other categories
-	Used Spring Boot to develop this API skeleton 
-	All the DTO to Model and Model to DTO conversion will take place in Service layer.
-	We will be using the Spring Data JPA to interact with database.
