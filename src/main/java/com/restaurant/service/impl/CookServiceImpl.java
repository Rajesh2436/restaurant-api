package com.restaurant.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restaurant.dto.ItemsDTO;
import com.restaurant.dto.OrderDTO;
import com.restaurant.model.OrderModel;
import com.restaurant.service.CookService;
import com.restaurant.service.OrderService;
import com.restaurant.util.PrepareItems;

@Service
public class CookServiceImpl implements CookService {

	@Autowired
	OrderService orderService;

	@Override
	public String cookItems(Integer categoryId, OrderDTO orderDetails) {

		// take items relevant for the category and put the data into Map
		List<Integer> listOfItems = new ArrayList<>();
		OrderModel orderModel = null;
		// convert OrderDTO to OrderModel

		// Update order based on the time required to cook item
		// check whether the categoryId is equal to the Snacks category if so
		// update the order at once after cooking all dishes
		// for simplicity hard coding snacks category id as 1
		if (categoryId == 1) {
			// find max waiting time for snacks
			Integer maxWait=0;
			for (ItemsDTO items : orderDetails.getOrderedItems()) {
				if (items.getCategoryId() == categoryId) {
					listOfItems.add(items.getProductId());
					if(maxWait< orderService.checkWaitingTime(items))
						maxWait=orderService.checkWaitingTime(items);
				}
			}
			PrepareItems prepareItems = new PrepareItems(listOfItems, maxWait, orderModel);
			prepareItems.start();
		}else{
			for (ItemsDTO items : orderDetails.getOrderedItems()) {
				if (items.getCategoryId() == categoryId) {
					PrepareItems prepareItems = new PrepareItems(items.getProductId(), orderService.checkWaitingTime(items), orderModel);
					prepareItems.start();
				}
			}
		}

		return "Cooking Started!!!";
	}

}
