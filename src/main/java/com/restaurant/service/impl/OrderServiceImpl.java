package com.restaurant.service.impl;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restaurant.dto.ItemsDTO;
import com.restaurant.dto.OrderDTO;
import com.restaurant.model.OrderModel;
import com.restaurant.repository.OrderRepository;
import com.restaurant.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderRepository orderRepository;
	
	@Override
	public ItemsDTO checkProductAvailability(ItemsDTO selecdtItem) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int checkWaitingTime(ItemsDTO selectedItem) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String placeOrder(OrderDTO orderPlaced) {

		int maxWaitingTime=0;
		OrderModel orderModel = null;
		
		// As far as i understand from your input and output sample you are just checking the max time to fully serve the dish
		// after this loop we will get maximum time taken to serve the order
		for(ItemsDTO item:orderPlaced.getOrderedItems()){
			int curWaitingTime = checkWaitingTime(item);
			if(maxWaitingTime < curWaitingTime) {
				maxWaitingTime = curWaitingTime;
			}
		}
		
		// DTO to Model conversions will happen in Service layer - OrderDTO to OrderModel
		
		// Once its done we will save the model using following code
		orderModel = orderRepository.save(orderModel);
		
		if(orderModel!=null){
			// find order fully served time using orderTime and maxWaitingTime
			
			Timestamp deliveryTime = new Timestamp(orderPlaced.getOrderTime().getTime() + (maxWaitingTime*60*1000));
			Date deliveryDate = new Date();
			deliveryDate.setTime(deliveryTime.getTime());
			return "Your order placed successfully and it will be fully served at : "+ new SimpleDateFormat("HH:mm").format(deliveryDate);
		}else{
			return "Your order not placed! please try again!";
		}
		
	}

	@Override
	public String updateOrder(OrderDTO orderPlaced) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String cancelOrder(Integer orderId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String cancelItem(Integer orderId, Integer productId) {
		// TODO Auto-generated method stub
		return "hiiii";
	}

	@Override
	public OrderDTO viewOrder() {
		// TODO Auto-generated method stub
		return null;
	}

}
