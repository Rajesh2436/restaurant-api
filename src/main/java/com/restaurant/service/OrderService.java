package com.restaurant.service;

import com.restaurant.dto.ItemsDTO;
import com.restaurant.dto.OrderDTO;

public interface OrderService {
	
	public ItemsDTO checkProductAvailability(ItemsDTO selecdtItem);
	
	public int checkWaitingTime(ItemsDTO selectedItem);
	
	public String placeOrder(OrderDTO orderPlaced);
	
	public String updateOrder(OrderDTO orderPlaced);
	
	public String cancelOrder(Integer orderId);
	
	public String cancelItem(Integer orderId, Integer productId);
	
	public OrderDTO viewOrder();

}
