package com.restaurant.service;

import java.util.List;

import com.restaurant.dto.MenuItemsDTO;


public interface MenuService {
	
	public List<MenuItemsDTO> viewMenu();
	
	public MenuItemsDTO addMenuItems(MenuItemsDTO newMenuItems);
	
	public String deleteMenuItems(MenuItemsDTO delMenuItems) ;
}
