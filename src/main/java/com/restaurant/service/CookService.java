package com.restaurant.service;

import com.restaurant.dto.OrderDTO;

public interface CookService {
	
	public String cookItems(Integer categoryId,OrderDTO orderDetails);

}
