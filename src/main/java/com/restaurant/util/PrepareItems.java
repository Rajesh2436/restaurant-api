package com.restaurant.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.restaurant.model.OrderModel;
import com.restaurant.model.ProductModel;
import com.restaurant.repository.OrderRepository;

public class PrepareItems extends Thread {

	@Autowired
	OrderRepository orderRepository;

	Integer productId;
	Integer productWaitTime;
	List<Integer> listOfItems;
	OrderModel orderModel;

	public PrepareItems(Integer productId, Integer productWaitTime, OrderModel orderModel) {
		this.productId = productId;
		this.productWaitTime = productWaitTime;
		this.orderModel = orderModel;
	}

	public PrepareItems(List<Integer> listOfItems, Integer maxWait, OrderModel orderModel) {
		this.listOfItems = listOfItems;
		this.productWaitTime = maxWait;
		this.orderModel = orderModel;
	}

	public void run() {
		if (this.listOfItems == null) {
			cookOtherCateItems();
		} else {
			cookSnacks();
		}
	}

	private void cookOtherCateItems() {
		// update the other category items when its cooked and update the
		// prodServed to true

		try {
			Thread.sleep(productWaitTime * 1000);

			for (ProductModel items : orderModel.getPoductList()) {
				if (items.getProdId() == productId) {
					items.setProdServed(true);
					orderRepository.updateOrder(orderModel);
					break;
				}
			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void cookSnacks() {
		// update the snacks category items when everything is cooked and update
		// the prodServed to true for all

		for (Integer productId : listOfItems) {
			try {
				Thread.sleep(productWaitTime * 1000);

				for (ProductModel items : orderModel.getPoductList()) {
					if (items.getProdId() == productId) {
						items.setProdServed(true);
						break;
					}
				}

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		orderRepository.updateOrder(orderModel);
		
	}

}
