package com.restaurant.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="category")
public class CategoryModel {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	Integer categoryId;
	
	String categoryName;
	
	List<ProductModel> prodList;
	
	Integer numOfChefs;

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	@OneToMany
	@JoinColumn(name = "categoryId")
	public List<ProductModel> getProdList() {
		return prodList;
	}

	public void setProdList(List<ProductModel> prodList) {
		this.prodList = prodList;
	}

	public Integer getNumOfChefs() {
		return numOfChefs;
	}

	public void setNumOfChefs(Integer numOfChefs) {
		this.numOfChefs = numOfChefs;
	}	

	
}
