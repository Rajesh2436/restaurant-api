package com.restaurant.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Order")
public class OrderModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Integer orderId;

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Timestamp getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(Timestamp orderTime) {
		this.orderTime = orderTime;
	}

	@OneToMany
	@JoinColumn(name = "categoryId")
	public List<ProductModel> getPoductList() {
		return poductList;
	}

	public void setPoductList(List<ProductModel> poductList) {
		this.poductList = poductList;
	}

	Integer customerId;

	Timestamp orderTime;

	List<ProductModel> poductList;

}
