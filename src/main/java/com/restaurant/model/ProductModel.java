package com.restaurant.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="product")
public class ProductModel {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	Integer prodId;
	
	String prodName;
	
	Integer categoryId;
	
	Integer prodPrepTime;
	
	Double prodRate;
	
	boolean prodServed;
	
	public boolean isProdServed() {
		return prodServed;
	}

	public void setProdServed(boolean prodServed) {
		this.prodServed = prodServed;
	}

	public Integer getProdId() {
		return prodId;
	}

	public void setProdId(Integer prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public Integer getProdPrepTime() {
		return prodPrepTime;
	}

	public void setProdPrepTime(Integer prodPrepTime) {
		this.prodPrepTime = prodPrepTime;
	}

	public Double getProdRate() {
		return prodRate;
	}

	public void setProdRate(Double prodRate) {
		this.prodRate = prodRate;
	}

}
