package com.restaurant.dto;

import java.util.List;

public class MenuItemsDTO {
	
	Integer categoryId;
	
	String categoryName;
	
	List<ProductDTO> prodList;
	
}
