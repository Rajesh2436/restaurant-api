package com.restaurant.dto;

import java.sql.Timestamp;
import java.util.List;

public class OrderDTO {
	
	Integer orderId;
	
	Integer customerId;	
	
	Timestamp orderTime;
	
	List<ItemsDTO> orderedItems;

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Timestamp getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(Timestamp orderTime) {
		this.orderTime = orderTime;
	}

	public List<ItemsDTO> getOrderedItems() {
		return orderedItems;
	}

	public void setOrderedItems(List<ItemsDTO> orderedItems) {
		this.orderedItems = orderedItems;
	}
	
	
	
}
