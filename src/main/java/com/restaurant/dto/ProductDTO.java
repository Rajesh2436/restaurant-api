package com.restaurant.dto;

public class ProductDTO {
	
	Integer prodId;
	
	String prodName;
	
	Integer categoryId;
	
	Integer prodPrepTime;
	
	Double prodRate;

}
