package com.restaurant.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.restaurant.dto.ItemsDTO;
import com.restaurant.dto.OrderDTO;
import com.restaurant.service.MenuService;
import com.restaurant.service.OrderService;

@RestController
public class CustomerController {

	// register user, order item, modify user details etc.
	
	@Autowired 
	OrderService orderService;
	
	@Autowired
	MenuService menuService;
	
	@RequestMapping(value="/check/available", method=RequestMethod.POST)
	public ItemsDTO checkProductAvailability(@RequestBody ItemsDTO selecdtItem) {
		// check whether a product is availble or not 
		// if available returns the item else return null
		return orderService.checkProductAvailability(selecdtItem);
	}

	@RequestMapping(value="/check/time", method=RequestMethod.POST)
	public int checkWaitingTime(@RequestBody ItemsDTO selectedItem) {
		// check the waiting time for each dish
		// returns the waiting time
		return orderService.checkWaitingTime(selectedItem);
	}

	@RequestMapping(value="/order", method=RequestMethod.POST)
	public String placeOrder(@RequestBody OrderDTO orderPlaced) {
		// Places the order and returns the waiting time for that order
		return orderService.placeOrder(orderPlaced);
	}
	
	@RequestMapping(value="/order", method=RequestMethod.PUT)
	public String updateOrder(@RequestBody OrderDTO orderPlaced) {
		// update the existing order 
		return orderService.updateOrder(orderPlaced);
	}
	
	@RequestMapping(value="/order/cancel/{orderId}", method=RequestMethod.DELETE)
	public String cancelOrder(@PathVariable Integer orderId) {
		// cancel an order 
		return orderService.cancelOrder(orderId);
	}

	
	@RequestMapping(value="/order/cancel/{orderId}/{productId}", method=RequestMethod.DELETE)
	public String cancelItem(@PathVariable Integer orderId, @PathVariable Integer productId) {
		// cancel a particular item in order
		return orderService.cancelItem(orderId, productId);
	}


	
}
