package com.restaurant.controller;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController {
	// the things that admin triggers will go here
	// ie. add/modify category, product etc. 
}
